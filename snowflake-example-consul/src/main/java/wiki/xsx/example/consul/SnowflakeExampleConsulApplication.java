package wiki.xsx.example.consul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xsx
 * @date 2019/11/22
 * @since 1.8
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SnowflakeExampleConsulApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnowflakeExampleConsulApplication.class, args);
    }

}
