package wiki.xsx.example.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xsx
 * @date 2019/11/22
 * @since 1.8
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SnowflakeExampleEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnowflakeExampleEurekaApplication.class, args);
    }

}
