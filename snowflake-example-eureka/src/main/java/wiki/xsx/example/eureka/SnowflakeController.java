package wiki.xsx.example.eureka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wiki.xsx.core.snowflake.config.Snowflake;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xsx
 * @date 2019/11/22
 * @since 1.8
 */
@RestController
@RequestMapping("/id")
public class SnowflakeController {

    @Autowired
    private Snowflake snowflake;

    @GetMapping("/long")
    public Map<String, Object> getLong() {
        return this.getResult(this.snowflake.nextId());
    }

    @GetMapping("/string")
    public Map<String, Object> getString() {
        return this.getResult(this.snowflake.nextIdStr());
    }

    private Map<String, Object> getResult(Object data) {
        Map<String, Object> result = new HashMap<>(3);
        result.put("code", 200);
        result.put("msg", "success");
        result.put("data", data);
        return result;
    }
}
